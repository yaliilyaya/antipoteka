$(function() {
    $('#stat .icon-item').mouseenter(function() {
        var t = $(this);
        t.addClass('hover');
        setTimeout(function() {
            t.removeClass('hover');
        }, 300);
    }).mouseleave(function() {
        $(this).removeClass('hover');
    });
});