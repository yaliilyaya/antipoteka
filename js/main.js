function CAnimateFon() {
    var t = this;
    this.obj = $(".top-fon .content-animation");
    this.startAnimate = function() {
        new CAnimate(this.obj, this.rand(1,4));
    };

    this.rand = function(min, max)
    {
        return Math.random() * (max - min) + min;
    };

    this.loop = function() {
        this.startAnimate();
        setTimeout(function() {t.loop()},this.rand(2000, 3000));
    };
    this.loop();
}
function CAnimate(box, c) {
    var t = this;
    this.c = parseInt(c);
    this.obj = $('<div>', {'class': 'fon-animate'});
    box.prepend(this.obj);
    setTimeout(function(){t.obj.addClass('active'+ t.c)}, 10);
    setTimeout(function(){t.obj.remove()}, 15000);
}

var WindowMessage = new function() {
    this.alert = function(mess) {
        return new Message(mess);
    };
    this.error = function(mess) {
        return new Message(mess, "error");
    };
    this.success = function(mess) {
        return new Message(mess, "success");
    };
};
function Message(mess, type) {
    var t = this;
    this.body = $('body');
    this.btn_close = $("<div>", {'class': "window-close"});
    this.box = $("<div>", {'class': "box-message"});
    this.background = $("<div>", {'class': "background"});
    this.window = $('<div>', {'class': "window-message " + type})
        .append(this.background)
        .append($('<div>', {'class': "box"})
            .append(this.btn_close)
            .append(this.box.append(mess))
        );
    this.open = function() {
        this.window.addClass("show");
    };
    this.close = function() {
        this.window.removeClass("show");
    };
    this.btn_close.click(function(){
        t.close();
    });
    this.background.click(function(){
        t.close();
    });
    this.body.append(this.window);
}

MainWindow = new function() {
    var t = this;
    this.construct = function() {

    };
    $(function(){
        t.construct();
    });
};
var BannerBox = new function () {
    var t = this;
    this.obj = null;
    this.box = null;
    this.nav_chain = null;
    this.index_banner = 0;
    this.auto_scroll = true;
    this.list = [];
    this.construct = function() {
        this.obj = $('.main-banner');
        this.box = $('.banner_content', this.obj);
        this.nav_chain = $('.banner_navigation', this.obj);
        $('input', this.nav_chain).each(function() {
            t.list.push(new Banner(this, t));
        });
        this.box.mouseenter(function(){
            t.auto_scroll = false;
        }).mouseleave(function(){
            t.auto_scroll = true;
        });
        setInterval(function() {
            t.nextBanner();
        }, 4000)
    };
    this.nextBanner = function() {
        if (!this.auto_scroll) {
            return;
        }
        this.index_banner++;
        if (this.index_banner >= this.list.length) {
            this.index_banner = 0;
        }
        this.show(this.list[this.index_banner]);
    };
    this.getObj = function(name) {
        return $(name, this.box);
    };
    this.show = function(banner) {
        for (var i in this.list) {
            if (this.list[i] == banner) {
                banner.show();
            }
            else {
                this.list[i].hide();
            }
        }
    };
    $(function() {
        t.construct();
    });
};
function Banner(field, main) {
    var t = this;
    this.main = main;
    this.field = $(field);
    this.name = this.field.attr("class");
    this.obj = this.main.getObj("."+ this.name);
    this.hide = function() {
        this.obj.removeClass("active");
        this.field.removeAttr("checked");
    };
    this.show = function () {
        this.obj.addClass("active");
        //this.field.click();
        this.field.attr("checked", "checked");
    };
    this.isActive = function () {
        return (this.field.attr("checked") == "checked");
    };
    this.field.click(function() {
        t.main.show(t);
    });
}
$(function(){
    new CAnimateFon();
    $('.main-banner .btn-show').click(function() {
        $('#base-main-menu').toggleClass('hidden');
    });
    $('.item-menu').click(function(){
        $('#mainmenu > ul').toggleClass('show');
        return false;
    });
});
