function CWindowInfo() {
    var t = this;
    this.obj = $('#window-info');
    this.btn = {
        close: $('.close', this.obj),
        run: $('.info .button', this.obj),
        next: $('.next-home'),
        prev: $('.prev-home')
    };
    this.offer_id = 0;
    this.preview = $('.preview', this.obj);
    this.move_active = false;
    this.list_img_box = $('.list-preview-box', this.obj)
    this.list_img = $('.list-preview', this.obj);
    this.info_box = $('.info ul.list', this.obj);
    this.info  = {
        ADDRESS: $("li:eq(0) span", this.info_box),
        metro: $("li:eq(1) span", this.info_box),
        AREA: $("li:eq(2) span", this.info_box),
        PRICE_M: $("li:eq(3) span", this.info_box),
        PRICE: $(".info .price", this.obj)
    };
    this.item_list = [];
    this.index_list = 0;

    this.list = [];
    this.form_apply = $('#apply_to form');

    this.show = function(offer_id) {
        this.offer_id = offer_id;
        this.index_list = this.getIndex(this.offer_id);
        this.loadData(offer_id);
    };
    this.loadData = function(offer_id) {
        $.get("/index.php/ApplyTo/GetItem/id/"+offer_id, {}, function(data) {
            if (typeof data == "object" && data != null) {
                t.postLoadData(data);
            }
        });
    };
    this.postLoadData = function(data) {
        var count = 0;
        this.list = [];
        this.list_img.empty();
        this.updateInfo(data);
        if (data['IMG'] != null) {
            for(var i in data['IMG']) {
                var img = $("<img/>", {
                    src: data['IMG'][i],
                    number_img:count++
                });
                this.list.push(img);
                var li = $('<li>').append(img).click(function(){
                    t.open($("img", $(this)).attr("number_img"));
                });
                this.list_img.append(li);
            }
        }
        this.open(0);
    };
    this.updateInfo = function(data) {
        for(var i in this.info) {
            if (data[i] != null) {
                this.info[i].html(data[i]);
            }
        }
    };
    this.open = function(ind) {
        console.log(ind);
        if (t.move_active) {
            return;
        }
        this.preview.html(this.list[ind].clone());
        $('li', this.list_img).removeClass("active");
        $(this.list[ind]).parent().addClass("active");
        t.obj.show();
        $('body').addClass('hidden');
    };
    this.close = function() {
        t.obj.hide();
        $('body').removeClass('hidden');
    };
    this.nextItem = function(i) {
        this.index_list += i;
        if (this.index_list >= this.item_list.length) {
            this.index_list = 0;
        }
        else if (this.index_list < 0) {
            this.index_list = this.item_list.length-1;
        }
        if (this.item_list.length > 0) {
            this.show(this.item_list[this.index_list]);
        }
    };
    this.getIndex = function (offer_id) {
        for (var i in this.item_list) {
            if (this.item_list[i] == offer_id) {
                return parseInt(i);
            }
        }
        return null;
    };
    $('#apply_to_list > .list > li').each(function(){
        var offer_id = $(this).attr("class").replace(/^.*?home\-group\-(\d+).*$/, "$1");
        offer_id = parseInt(offer_id);
        if (!isNaN(offer_id)) {
            t.item_list.push(offer_id);
        }
    });
    this.margin_left = 0;
    this.x = null;
    this.list_img_box.mousedown(function(){
        t.move_active = true;
        t.x = null;
    }).mouseup(function(){
        t.move_active = false;
        t.x = null;
    }).mouseleave(function(){
        t.move_active = false;
        t.x = null;
    }).mousemove(function(e) {
        if (!t.move_active) {
            return;
        }
        if (t.x != null) {
            t.margin_left += t.x - e.pageX;
            if (t.margin_left >= 0) {
                t.list_img.css("margin-left", (-t.margin_left)+"px");
            }
            else {
                t.margin_left = 0;
            }
        }

        t.x = e.pageX;
    });
    this.btn.close.click(function(){
        t.close();
    });
    this.btn.run.click(function(){
        t.form_apply.addClass('double-column');
        new CPreview($('.home-group-'+t.offer_id));
        setTimeout(function() {
            FormAppleTo.update();
        }, 100);
        t.close();
    });

    this.btn.next.click(function() {
        t.nextItem(1);
    });
    this.btn.prev.click(function() {
        t.nextItem(-1);
    });
}
function CPreview(obj) {
    var t = this;
    this.form_apply = $('#apply_to form');
    this.content = $('.home-param', this.form_apply);

    this.btn_clear = $('<a>', {'class': "close"});
    this.div = $('<div>', {'class': "item-preview"})
        .append(this.btn_clear);

    this.start = function() {
        this.content.html(obj.html());
        var el = $('.item-preview', this.content);
        this.div.append(el.html());
        el.replaceWith(this.div);
        this.form_apply.addClass('double-column');
        $('div.body').scrollTop(this.form_apply.offset().top);
    };
    this.clear = function () {
        this.form_apply.removeClass('double-column');
        this.content.empty();
    };

    this.btn_clear.click(function() {
        t.clear();
    });
    this.start();
}
function CFormAppleTo() {
    var t = this;
    this.obj = $("#apply_to, #feedback");
    this.form = $("form", this.obj);
    this.list_field = [];
    this.update = function() {
        this.list_field = [];//TODO:: Empty obj
        $('input, textarea', this.obj).each(function() {
            if ($(this).attr('type') != "submit") {
                t.list_field.push(new CFormFieldAppleTo(this));
            }
        });
    };
    this.form.submit(function() {
        for (var i in t.list_field) {
            if (!t.list_field[i].validate()) {
                return false;
            }
        }
    });
    this.update();
}
function CFormFieldAppleTo(obj) {
    this.field = $(obj);
    this.name = this.field.attr('name').replace(/APPLY_TO\[([A-Z_]+)\]/, "$1");

    this.type = this.field.attr('validate') + "";
    this.type = (this.type == "undefined" || this.type == "" ? "text" : this.type);
    this.validate = function() {
        this.field.removeClass("error");
        if (this['v'+this.type] != null && !this['v'+this.type](this.field.val())) {
            this.field.addClass("error");
            WindowMessage.error('Некоректное поле "' + this.field.attr('placeholder') + '"').open();
            return false;
        }
        return true;
    };
    this.vtext = function (val) {
        return val != "";
    };
    this.vemail = function(val) {
        if (val == "") {
            return false;
        }
        return (val.search(/^[\.\da-z]+@[\da-z]+\.[\da-z]+$/i) != -1);
    };
    this.vphone = function(val) {
        if (val == "") {
            return false;
        }
        return (val.search(/^[-\d\s]+$/i) != -1);
    };

}
var WindowInfo = null,
    FormAppleTo = null;

$(function() {
    WindowInfo = new CWindowInfo();
    FormAppleTo = new CFormAppleTo();
    $('.apply-to-list-next').click(function(){
        $(this).hide();
        $('#apply_to_list').addClass("full");

    });
    //WindowInfo.show(3);
});
