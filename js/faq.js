$(function(){
    $('#question_list.list li').click(function() {
        $(this).toggleClass('active');
    });
});
function PullBase() {
    var t = this;
    this.obj = $('#poll');
    this.question = [];
    $('.list li', this.obj).each(function(){
        t.question.push(new PullQuestion(this));
    });
}
function PullQuestion(obj) {
    var t = this;
    this.obj = $(obj);
    this.form = $('form', this.obj);
    this.answer = [];
    $('.answer div', this.obj).each(function(){
        t.answer.push(new PullAnswer(this, t));
    });
    $('.question', this.obj).click(function() {
        t.obj.toggleClass('active');
    });
    $('input', this.obj).change(function() {
        t.form.submit();
    });
}
function PullAnswer(obj, question) {

    var t = this;
    this.question = question;

    this.obj = $(obj);

    this.input = $("input", this.obj);
    this.obj.click(function(){
        //t.input.trigger("change").target('click');//TODO:: не работает click()
    });
}
var CPull = null;
$(function() {
    CPull = new PullBase();
});
