function in_diapozon(val, min,max) {
    if (min > val) {
        return min;
    }
    else if (max < val) {
        return max;
    }
    return val;
}
function Calc1() {
    var t = this;
    this.obj = $('#calc_1');
    this.slider = {
        year: $( ".year .slider .calc", t.obj ),
        sum: $( ".sum .slider .calc", t.obj )
    };
    this.year = 5;
    this.sum = 2500000;
    this.field = {
        year: $('.year td .input span', this.obj),
        sum: $('.sum td .input span', this.obj),
        field_1: $('.field_1', this.obj),
        field_2: $('.field_2', this.obj),
        field_3: $('.field_3', this.obj)
    };
    this.btn = {
        year: {
            left: $(".year .btn-left", this.obj),
            right: $(".year .btn-right", this.obj)
        },
        sum: {
            left: $(".sum .btn-left", this.obj),
            right: $(".sum .btn-right", this.obj)
        }
    };
    this.calc = function() {
        this.year = in_diapozon(this.year, 0, 24);
        this.sum = in_diapozon(this.sum, 0, 10000000);

        this.field.year.html(this.toPrice(this.year));
        this.field.sum.html(this.toPrice(this.sum));

        var C15 = this.sum * Math.pow( 1 + const_1, this.year);
        var C21 = this.sum * const_2;
        var C22 = C15 / (this.year * 12);

        this.field.field_1.html(this.toPrice(C21));
        this.field.field_2.html(this.toPrice(C22));
        this.field.field_3.html(this.toPrice(C21+C22));

    };
    this.toPrice = function(val) {
        val = parseInt(Math.round(val));
        return isNaN(val) ? 0 : (val+"").replace( /(?=\B(?:\d{3})+\b)/g, ' ' );
    };
    this.slider.year.slider({
        orientation: "horizontal",
        range: "min",
        min: 0,
        max: 24,
        value: this.year,
        animate: true,
        slide: function( event, ui ) {
            t.year = parseInt(ui.value);
            t.calc();
        }
    });
    this.slider.sum.slider({
        orientation: "horizontal",
        value: this.sum,
        range: "min",
        min: 0,
        max: 10000000,
        step: 100000,
        animate: true,
        slide: function( event, ui ) {
            t.sum = parseInt(ui.value);
            t.calc();
        }
    });
    setTimeout(function() {
        $("span", t.slider.year).removeAttr('tabindex');
        $("span", t.slider.sum).removeAttr('tabindex');
    }, 100);

    this.btn_controller = function(name, inc) {
        if (name == "year") {
            this.year += (inc ? 1 : -1);
        }
        else if (name == "sum") {
            this.sum += 100000 * (inc ? 1 : -1);
        }
        this.calc();
    };
    this.btn.year.left.click(function() {
        t.btn_controller("year", false);
    });
    this.btn.year.right.click(function() {
        t.btn_controller("year", true);
    });
    this.btn.sum.left.click(function() {
        t.btn_controller("sum", false);
    });
    this.btn.sum.right.click(function() {
        t.btn_controller("sum", true);
    });
    this.calc();
}
function Calc2() {
    var t = this;
    this.obj = $('#calc_2');
    this.year = 5;
    this.sum = 2500000;
    this.slider = {
        year: $( ".year .slider .calc", t.obj ),
        sum: $( ".sum .slider .calc", t.obj )
    };
    this.field = {
        year: $('.year td .input span', this.obj),
        sum: $('.sum td .input span', this.obj),
        field_1: $('.field_1', this.obj),
        field_2: $('.field_2', this.obj),
        field_3: $('.field_3', this.obj)
    };

    this.btn = {
        year: {
            left: $(".year .btn-left", this.obj),
            right: $(".year .btn-right", this.obj)
        },
        sum: {
            left: $(".sum .btn-left", this.obj),
            right: $(".sum .btn-right", this.obj)
        }
    };
    this.calc = function() {
        this.year = in_diapozon(this.year, 0, 24);
        this.sum = in_diapozon(this.sum, 0, 10000000);
        this.field.year.html(this.toPrice(this.year));
        this.field.sum.html(this.toPrice(this.sum));

        var C15 = this.sum * Math.pow( 1 + const_1, this.year);
        var C16 = this.sum * const_2 * this.year * 12;
        this.field.field_1.html(this.toPrice(C15));
        this.field.field_2.html(this.toPrice(C16));
        this.field.field_3.html(this.toPrice(C15 + C16));
    };
    this.toPrice = function(val) {
        return (Math.round(val)+"").replace( /(?=\B(?:\d{3})+\b)/g, ' ' );
    };
    this.slider.year.slider({
        orientation: "horizontal",
        range: "min",
        min: 0,
        max: 24,
        value: this.year,
        animate: true,
        slide: function( event, ui ) {
            t.year = parseInt(ui.value);
            t.calc();
        }
    });
    t.slider.sum.slider({
        orientation: "horizontal",
        value: this.sum,
        range: "min",
        min: 0,
        max: 10000000,
        step: 100000,
        animate: true,
        slide: function( event, ui ) {
            t.sum = parseInt(ui.value);
            t.calc();
        }
    });
    setTimeout(function() {
        $("span", t.slider.year).removeAttr('tabindex');
        $("span", t.slider.sum).removeAttr('tabindex');
    }, 100);
    this.btn_controller = function(name, inc) {
        if (name == "year") {
            this.year += (inc ? 1 : -1);
        }
        else if (name == "sum") {
            this.sum += 100000 * (inc ? 1 : -1);
        }
        this.calc();
    };
    this.btn.year.left.click(function() {
        t.btn_controller("year", false);
    });
    this.btn.year.right.click(function() {
        t.btn_controller("year", true);
    });
    this.btn.sum.left.click(function() {
        t.btn_controller("sum", false);
    });
    this.btn.sum.right.click(function() {
        t.btn_controller("sum", true);
    });
    this.calc();
}

var const_1 = 0.06;
var const_2 = 0.007;
$(function(){
    if ($('#calc_1').length) {
        new Calc1();
    }
    if ($('#calc_2').length) {
        new Calc2();
    }
});
