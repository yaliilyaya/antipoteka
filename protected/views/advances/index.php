<div id="stat" class="section">
    <div class="top-line"></div>
    <ul class="item_list">
        <?
        foreach ($list as $elem) {
            ?>
            <li>
                <div class="item-title"><span><?=$elem->NAME?></span></div>
                <i class="icon-item ">
                    <img src="<?=$elem->getPathFile("IMG")?>">
                </i>
                <div class="item-count"><?=$elem->TEXT?></div>
            </li>
            <?
        }
        ?>
    </ul>
    <div class="bottom-line"></div>
</div>