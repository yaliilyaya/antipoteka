<div id="calc_1" class="section calculator bottom-left">
    <div>
        <div class="title">Калькулятор расчёта стоимости платежей за квартиру</div>
        <div class="form">
            <table>
                <tr class="sum">
                    <td>Стоимость квартиры</td>
                    <td class="slider"><div>
                            <div  class="calc"></div>
                        </div></td>
                    <td>
                        <div class="input calc-btn">
                            <span ></span> руб
                            <div class="btn-left"></div>
                            <div class="btn-right"></div>
                        </div>
                    </td>
                </tr>
                <tr class="year">
                    <td>Срок</td>
                    <td class="slider"><div>
                            <div class="calc"></div>
                        </div></td>
                    <td><div class="input calc-btn">
                            <span></span> лет
                            <div class="btn-left"></div>
                            <div class="btn-right"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="title-row"><div>Арендная плата:</div></td>
                    <td><span class="field_1">28 000</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="title-row" ><div>Взнос на выкуп квартиры:</div></td>
                    <td><span class="field_2">55 894</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="title-row"><div>итого ежемесячный платёж:</div></td>
                    <td><span class="field_3">83 894</span></td>
                </tr>
            </table>
        </div>
    </div>
</div>