<div id="calc_2" class="section calculator bottom-left">
    <div>
        <div class="title">Калькулятор расчёта доходности</div>
        <div class="form">
            <table>
                <tr class="sum">
                    <td >Сумма вложения</td>
                    <td class="slider"><div><div class="calc"></div></div></td>
                    <td >
                        <div class="input calc-btn">
                            <span >2 500 000</span> руб
                            <div class="btn-left"></div>
                            <div class="btn-right"></div>
                        </div>

                        </td>
                </tr>
                <tr class="year">
                    <td >Срок</td>
                    <td class="slider"><div><div class="calc"></div></div></td>
                    <td >
                        <div class="input calc-btn">
                            <span>5</span> лет
                            <div class="btn-left"></div>
                            <div class="btn-right"></div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td ></td>
                    <td class="title-row"><div>Выплаты (руб):</div></td>
                    <td ><span class="field_1">55 894</span></td>
                </tr>
                <tr>
                    <td ></td>
                    <td class="title-row"><div>IRR:</div></td>
                    <td ><span class="field_2">83 894</span></td>
                </tr>
            </table>
        </div>
    </div>
    <script>

    </script>
</div>