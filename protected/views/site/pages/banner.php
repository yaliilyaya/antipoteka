<??>
<div class="main-banner">
    <div class="btn-show"></div>
    <ul class="banner_content">
        <?
        $count = 0;
        foreach ($list as $banner) {
            ?>
            <li class="banner_<?=$banner->ID?> <?=(!$count++ ? "active" : "")?>"
                style="background-image: url('<?=$banner->getPathFile("IMG")?>')">
                <span><?=$banner->TEXT?></span>
            </li>
            <?
        }
        ?>
    </ul>
    <div class="banner_navigation">
        <?
        $count = 0;
        foreach ($list as $banner) {
            ?>
            <input type="radio" name="banner" class="banner_<?=$banner->ID?>" <?=(!$count++ ? 'checked="checked"' : "")?>/>
            <?
        }
        ?>
    </div>
</div>