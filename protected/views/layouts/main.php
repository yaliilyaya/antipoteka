<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <script src="//code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>

    <?
    $this->registerCss("/css/main.css");

    $this->registerCss("//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css", false);

    $this->registerJs("//code.jquery.com/jquery-1.10.2.js", false);
    $this->registerJs("//code.jquery.com/ui/1.11.4/jquery-ui.js", false);
    $this->registerJs("/js/main.js");
    $this->registerJs("//vk.com/js/api/openapi.js?116", false);
    ?>
    <title><?=$this->getPageTitle()?></title>
</head>
<body class="page_<?=$this->getAction()->getId()?>">
    <div class="body">
    <?
    if (Yii::app()->mess->isError()) {
        Yii::app()->mess->printError(Yii::app()->mess->getError());
    }
    else if (Yii::app()->mess->isSuccess()) {
        Yii::app()->mess->printSuccess(Yii::app()->mess->getSuccess());
    }
    ?>
    <div id="base-main-menu" <?=(($this->getId() == "site" && $this->getAction()->getId() == "index") ? "" : 'class="hidden"')?>>
        <div class="top-fon">
            <div class="content-animation"></div>
            <div class="content">

                <a class="faq" href="/index.php/site/faq">F.A.Q.</a>
                <div class="fon-logo">
                <a class="icon-logo" href="/"></a>
                </div>
            </div>
            <div class="top-line"></div>

        </div>
        <div id="mainmenu">
                <?php $this->widget('zii.widgets.CMenu',array(
                    'items'=>array(
                        array('label'=>'Главная', 'url'=>array('/')),
                        array('label'=>'наши предложения', 'url'=>array('site/supply')),
                        array('label'=>'Информация для инвесторов', 'url'=>array('site/info')),
                        array('label'=>'Контакты', 'url'=>array('site/contact'), ),
                    ),
                ));?>
                <span class="icon-phone" >8 800 000 00 00</span>
                <a href="/" class="item-menu">МЕНЮ</a>
        </div>

        <?ERunActions::runAction("Site/RunBanner");?>
    </div>
        <!-- mainmenu -->
    <?php echo $content; ?>
    </div>
<?$this->renderPartial('//site/pages/ya_metrika');?>
</body>
</html>