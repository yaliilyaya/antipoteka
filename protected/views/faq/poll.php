<div id="poll">
    <div class="title">Опросы</div>
    <ul class="list">
        <?
        foreach ($pull_list as $pull) {
            ?>
            <li>
                <form method="post">
                    <div class="question">
                        <span><?=$pull->TEXT?></span>
                    </div>
                    <div class="answer">
                        <?
                        foreach ($pull->ANSWER as $answer) {
                            ?>
                            <div>
                                <?
                                if ($pull->RUN) {
                                    ?><?=ceil($answer->COUNT / $pull->COUNT * 100)?>%<?
                                }
                                else {
                                    ?>
                                    <input type="radio" name="QUESTION[<?=$pull->ID?>]" value="<?=$answer->ID?>">
                                    <?
                                }
                                ?>
                                <?=$answer->TEXT?>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </form>
            </li>
            <?
        }
        ?>
    </ul>
</div>
