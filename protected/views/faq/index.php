<div id="faq" class="section">
    <div>
        <div class="title">F.A.Q.</div>
        <ul id="question_list" class="list">
            <?
            foreach ($list as $elem)  {
                ?>
                <li >
                    <div class="question">
                        <span>Вопрос:</span>
                        <?=$elem->TEXT?>
                    </div>
                    <div class="answer">
                        <span>Ответ:</span>
                        <?=$elem->ANSWER?>
                    </div>
                </li>
                <?
            }
            ?>
        </ul>
    </div>
    <?
    ERunActions::runAction("Faq/RunPoll");
    ?>
</div>