<?
$offer_show = [];
$offer_hide = [];
$count = 0;
foreach ($offer_list as $offer) {
    if ($count++ < 4 ) {
        $offer_show[] = $offer;
    }
    else {
        $offer_hide[] = $offer;
    }
}
?>

<div id="apply_to_list" class="section bottom-left">
    <div class="title">Готовые квартиры</div>
    <ul class="list" >
        <?
        foreach ($offer_show as $offer) {
            ?>
            <li class="home-param home-group-<?=$offer->ID?>">
                <input type="HIDDEN" name="APPLY_TO[OFFER_ID]" value="<?=$offer->ID?>">
                <a class="item-preview" onclick="WindowInfo.show(<?=$offer->ID?>);">
                    <img src="<?=$offer->IMG->getPathFile("IMG")?>">
                </a>
                <ul class="list">
                    <li class="item-address">
                        <?=$offer->ADDRESS?>
                    </li>
                    <li class="item-param">
                        <label>Площадь:</label>
                        <span><?=$offer->AREA?> кв.м</span>
                        <div></div>
                    </li>
                    <li class="item-param">
                        <label>Цена за метр:</label>
                        <span><?=$offer->PRICE_M?>р</span>
                        <div></div>
                    </li>
                    <li class="item-price">
                        Цена: <?=$offer->PRICE?>р
                    </li>
                </ul>
            </li>
            <?
        }
        ?>
    </ul>
    <?
    if (count($offer_hide)) {
        ?>
        <ul class="list list-next" >
            <?
            foreach ($offer_hide as $offer) {
                ?>
                <li class="home-param home-group-<?=$offer->ID?>">
                    <a class="item-preview" onclick="WindowInfo.show(<?=$offer->ID?>);">
                        <img src="<?=$offer->IMG->getPathFile("IMG")?>">
                    </a>
                    <ul class="list">
                        <li class="item-address">
                            <?=$offer->ADDRESS?>
                        </li>
                        <li class="item-param">
                            <label>Площадь:</label>
                            <span><?=$offer->AREA?> кв.м</span>
                            <div></div>
                        </li>
                        <li class="item-param">
                            <label>Цена за метр:</label>
                            <span><?=$offer->PRICE_M?>р</span>
                            <div></div>
                        </li>
                        <li class="item-price">
                            Цена: <?=$offer->PRICE?>р
                        </li>
                    </ul>
                </li>
            <?
            }
            ?>
        </ul>
        <a class="apply-to-list-next">Показать ещё</a>
    <?
    }
    ?>
</div>