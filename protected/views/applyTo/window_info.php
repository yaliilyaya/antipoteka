<?
    $img_list = array(
        "/img/content/home11.jpg",
        "/img/content/home21.jpg",
        "/img/content/home31.jpg",
        "/img/content/home41.jpg",
        "/img/content/home51.jpg",
    );
?>

<div id="window-info">
    <div class="window">
        <div class="close btn-nav" title="Закрыть"></div>
        <div class="next-home btn-nav" title="Следущий элемент"></div>
        <div class="prev-home btn-nav" title="Предыдущий элемент"></div>
        <div class="preview">
            <img src="/img/content/house-big.jpg">
        </div>
        <div class="list-preview-box">
            <ul class="list-preview list">
                <?
                foreach ($img_list as $img) {
                    ?>
                    <li><img src="<?=$img?>"></li>
                    <?
                }
                ?>
            </ul>
        </div>
        <div class="info">
            <div class="price">1 410 000 р</div>
            <ul class="list">
                <li>
                    Адрес:
                    <span>Нижний Новгород, пр-т Гагарина dasd as</span>
                </li>
                <li>
                    Метро рядом:
                    <span>Электроза</span>
                </li>
                <li>
                    Площадь:
                    <span>120 кв.м.</span>
                </li>
                <li>
                    Цена за метр:
                    <span>6 900р</span>
                </li>
            </ul>
            <div class="button">Беру</div>
        </div>
    </div>
</div>