<div class="panel panel-default">
    <div class="panel-heading">
        <div class="btn-group"  style="float: right">
            <a class="btn btn-primary btn-social"  href="<?=$this->url("/admin/{$this->name}/")?>">
                <i class="fa  fa-arrow-up"></i> Опросы</a>
            <a class="btn btn-primary" href="<?=$this->url("/admin/{$this->name}/answer_add/pull_id/{$pull_id}")?>">
                <i class="fa fa-plus"></i></a>

        </div>

        Список элементов
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <?
                    foreach ($this->getClass()->attributeLabels() as $name) {
                        ?>
                        <th><?=$name?></th>
                    <?
                    }
                    ?>
                    <th width="100">Опции</th>
                </tr>
                </thead>
                <?
                if (count($list)) {
                    ?>
                    <tbody>
                    <?
                    foreach ($list as $row) {
                        ?>
                        <tr>
                            <?
                            foreach ($row->attributeLabels() as $key => $elem) {
                                ?>
                                <td><?=$row->$key?></td>
                            <?
                            }
                            ?>
                            <td width="100">
                                <div class="btn-group" >
                                    <a class="btn btn-primary" href="/index.php/admin/<?=$this->name?>/answer_edit/id/<?=$row->ID?>">
                                        <i class="fa  fa-pencil"></i></a>
                                    <a class="btn btn-primary" href="/index.php/admin/<?=$this->name?>/answer_delete/id/<?=$row->ID?>"
                                       onclick="return confirm('Вы уверены что хотите удалить');">
                                        <i class="fa fa-bitbucket"></i></a>
                                </div>
                            </td>
                        </tr>
                    <?
                    }
                    ?>
                    </tbody>
                <?
                }
                ?>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>
<?
