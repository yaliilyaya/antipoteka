<form method="post">
    <input type="hidden" name="EDIT" value="Y">
    <?
    foreach ($value->attributeLabels() as $key => $name) {
        if ($value->getType($key) == "text") {
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?=$name?>
                </div>
                <div class="panel-body">
                    <?=$value->$key?>
                </div>
            </div>
        <?
        }
        else {
            ?>
            <div class="form-group input-group">
                <span class="input-group-addon"><?=$name?></span>
                <span class="form-control"><?=$value->$key?></span>
            </div>
            <?
        }
    }
?>
    <a class="btn btn-social btn-primary" href="/index.php/admin/<?=$this->name?>/edit/id/<?=$value->ID?>">
        <i class="fa  fa-pencil"></i>Редактировать</a>
</form>
