<? $form=$this->beginWidget('CActiveForm',array(
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
    <input type="hidden" name="EDIT" value="Y">
    <?
    foreach ($value->attributeLabels() as $key => $name) {
        if ($value->getType($key) == "file") {
            ?>
            <div class="form-group input-group">
                <?=$form->labelEx($value,$key, ['class' => "input-group-addon"])?>
                <?=$form->fileField($value,$key, ['class' => "form-control"])?>
            </div>
        <?
        }
        else if ($value->getType($key) == "text") {
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?=$form->labelEx($value,$key)?>
                </div>

                <?=$form->textArea($value,$key, ['class' => "panel-body form-control"])?>

            </div>
        <?
        }
        else {
            ?>
            <div class="form-group input-group">
                <?=$form->labelEx($value,$key, ['class' => "input-group-addon"])?>
                <?=$form->textField($value,$key, ['class' => "form-control"])?>
            </div>
            <?
        }
    }
?>
<?=CHtml::submitButton("Добавить", ['class' => "btn btn-primary"])?>

<?php $this->endWidget(); ?>
