
    <div class="panel panel-default">
        <div class="panel-heading">
            Список элементов
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <?
                        foreach ($this->getClass()->attributeLabels() as $name) {
                            ?>
                            <th><?=$name?></th>
                        <?
                        }
                        ?>
                    </tr>
                    </thead>
                    <?
                        if (count($list)) {
                    ?>
                    <tbody>
                        <?
                        foreach ($list as $row) {
                            ?>
                            <tr>
                                <?
                                foreach ($row->attributeLabels() as $key => $elem) {
                                    ?>
                                    <td><?=$row->$key?></td>
                                <?
                                }
                                ?>
                            </tr>
                        <?
                        }
                        ?>
                    </tbody>
                    <?
                        }
                    ?>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <?
