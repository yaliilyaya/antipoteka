<?
if (isset($this->name_list)) {
    ?>
    <form method="post">
        <input type="hidden" name="ADD" value="Y">
        <?
        foreach ($this->name_list as $key => $name) {
            if ($key == "ID") {
                continue;
            }
            ?>
            <div class="form-group input-group">
                <span class="input-group-addon"><?=$name?></span>
                <input type="text" name="<?=$key?>" class="form-control">
            </div>
        <?
        }
    ?>
    <button class="btn btn-default" type="submit">Добавить</button>
    </form>
<?
}