<?
$name_controller = $this->name;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <a class="btn btn-primary" style="float: right" href="<?=$this->url("/admin/".$this->name."/add")?>">
            <i class="fa fa-plus"></i></a>
        Список элементов
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table">
                <?
                if (isset($this->name_list)) {
                    $name_list = &$this->name_list;
                    $name_list = array_merge($name_list, ["Опции"]);
                    ?>
                    <thead>
                    <tr>
                        <?
                        foreach ($name_list as $name) {
                            ?>
                            <th><?=$name?></th>
                        <?
                        }
                        ?>
                    </tr>
                    </thead>
                    <?
                }?>
                <?
                if (count($list)) {
                    ?>
                    <tbody>
                        <?
                        foreach ($list as $row) {
                            ?>
                            <tr>
                                <?
                                foreach ($row as $key => $elem) {
                                    ?>
                                    <td><?=$elem?></td>
                                    <?
                                }
                                ?>
                                <td width="100">
                                    <div class="btn-group" >
                                        <a class="btn btn-primary" href="/index.php/admin/<?=$name_controller?>/edit/id/<?=$row['ID']?>">
                                            <i class="fa  fa-pencil"></i></a>
                                        <a class="btn btn-primary" href="/index.php/admin/<?=$name_controller?>/delete/id/<?=$row['ID']?>"
                                            onclick="return confirm('Вы уверены что хотите удалить');">
                                            <i class="fa fa-bitbucket"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?
                        }
                        ?>
                    </tbody>
                <?
                }?>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->
