<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminViewElemAction extends Action {
    function run() {
        $this->controller->render("/default/view_elem", [
                'value' =>$this->controller->getClass()->find()
            ]
        );
    }
}