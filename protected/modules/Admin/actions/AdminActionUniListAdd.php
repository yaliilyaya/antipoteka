<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminActionUniListAdd extends Action {
    function run() {
        $this->controller->setPageTitle(implode(" - ", [
            $this->controller->getPageTitle(),
            "Добавление"
        ]));

        if (arrVal($_POST, "ADD") === "Y") {
            $param = [];
            foreach (array_keys($this->controller->name_list) as $key) {
                $val = arrVal($_POST, $key);
                $param['TYPE'] = $this->controller->name;
                if ($val != null) {
                    $param[$key] = $val;
                }
            }
            Yii::app()->db->createCommand()
                ->insert($this->controller->table, $param);
            $this->controller->redirect($this->url('/admin/'.$this->controller->name));
        }
        $this->controller->render('/default/unilist_add', []);
    }
}