<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminViewList2Action extends Action {
    function run() {
        $this->controller->render("/default/view_list2", [
                'list' =>$this->controller->getClass()->findAll()
            ]
        );
    }
}