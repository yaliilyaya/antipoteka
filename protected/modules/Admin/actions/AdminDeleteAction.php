<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminDeleteAction extends Action {
    function run($id) {
        $model = $this->controller->getClass()->findByPk($id);
        if ($model->delete()) {
            $this->controller->redirect($this->url('/admin/'.$this->controller->name));
        }
    }
}