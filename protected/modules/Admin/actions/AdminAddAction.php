<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminAddAction extends Action {
    function run() {
        $this->controller->setPageTitle(implode(" - ", [
            $this->controller->getPageTitle(),
            "Добавление"
        ]));
        $model = $this->controller->getClass();
        $attributes = arrVal($_POST, CHtml::modelName($model));
        if ($attributes != null) {
            $model->attributes = $attributes;
            if ($model->save()) {
                $this->controller->redirect($this->url("/admin/".$this->controller->name));
            }
        }
        $this->controller->render("/default/add_elem", [
                'value' =>$model
            ]
        );

    }
}