<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminEditAction extends Action {
    function run($id) {
        $this->controller->setPageTitle(implode(" - ", [
            $this->controller->getPageTitle(),
            "Редактирование"
        ]));
        $model = $this->controller->getClass();

        $attributes = arrVal($_POST, CHtml::modelName($model));
        if ($attributes != null) {
            $model = $model->findByPk($id);

            $attributes_old = $model->getAttributes();
            $model->attributes = $attributes;

            foreach ($model->getAttributeByType("file") as $attribute) {
                if (empty($model->$attribute) && !empty($attributes_old[$attribute])) {
                    $model->$attribute = $attributes_old[$attribute];
                }
            }
            if ($model->save()) {
                $this->controller->redirect($this->url("/admin/".$this->controller->name));
            }
        }
        else {
            $model = $model->findByPk($id);
        }
        $this->controller->render("/default/edit_elem", [
                'value' =>$model
            ]
        );

    }
}