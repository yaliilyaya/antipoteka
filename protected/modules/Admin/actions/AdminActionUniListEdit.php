<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminActionUniListEdit extends Action {
    function run($id) {
        $this->controller->setPageTitle(implode(" - ", [
            $this->controller->getPageTitle(),
            "Редактирование"
        ]));
        if (arrVal($_POST, "EDIT") === "Y") {
            $param = [];
            foreach (array_keys($this->controller->name_list) as $key) {
                $val = arrVal($_POST, $key);
                if ($val != null) {
                    $param[$key] = $val;
                }
            }
            Yii::app()->db->createCommand()
                ->update($this->controller->table,
                    $param,
                    ["AND", "TYPE = :type", "ID = :id"],
                    [':type'=> $this->controller->name, ':id' => $id]);
            $this->controller->redirect($this->url('/admin/'.$this->controller->name));
        }
        $query = Yii::app()->db->createCommand()
            ->select(["ID", "NAME", "VALUE"])
            ->from($this->controller->table)
            ->where(["AND", "TYPE = :type", "ID = :id"], [':type'=> $this->controller->name, ':id' => $id]);

        $this->controller->render('/default/unilist_edit', [
            'value' => $query->queryRow()
        ]);
    }
}