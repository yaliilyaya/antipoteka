<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminActionUniListDelete extends Action {
    function run($id) {
        Yii::app()->db->createCommand()
            ->delete(
                $this->controller->table,
                [
                    "AND",
                    "TYPE = :type",
                    "ID = :id"
                ],
                [
                    ':type'=> $this->controller->name,
                    ':id' => intval($id)
                ]
            );
        $this->controller->redirect($this->url('/admin/'.$this->controller->name));
    }
}