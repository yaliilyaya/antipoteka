<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:16
 */

class AdminActionUniList extends Action {
    function run() {
        $query = Yii::app()->db->createCommand()
            ->select(["ID", "NAME", "VALUE"])
            ->from($this->controller->table)
            ->where("TYPE = :type", [':type'=> $this->controller->getName()]);

        $this->controller->render('/default/unilist_list', [
            'list' => $query->queryAll()
        ]);
    }
}