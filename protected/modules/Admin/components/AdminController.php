<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 4:49
 */

class AdminController extends Controller {
    public $model = "";
    public function getTableRow($id) {
        return Yii::app()->db->createCommand()
            ->from($this->table)
            ->where("ID = :id", [':id' => $id])
            ->queryRow();
    }

    /**
     * @return CActiveRecord
     */
    public function getClass() {
        return new $this->model;
    }
} 