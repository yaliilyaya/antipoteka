<?php

class AdvancesController extends AdminController
{
    public $model = "Advances";

    public function actions() {
        $this->setPageTitle("Преемущества");
        return [
            'index' => "AdminViewListAction",
            'add' => "AdminAddAction",
            'delete' => "AdminDeleteAction",
            'edit' => "AdminEditAction",
        ];
    }
}