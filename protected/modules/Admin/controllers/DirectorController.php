<?php

class DirectorController extends AdminController
{
    public $model = "Director";
    public function actions() {
        $this->setPageTitle("Директор");
        return [
            'index' => "AdminViewElemAction",
            'add' => "AdminActionUniListAdd",
            'delete' => "AdminActionUniListDelete",
            'edit' => "AdminEditAction",
        ];
    }
}