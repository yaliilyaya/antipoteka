<?php

class InvestorController extends Controller
{
    public $table = "unilist";
    public $name_list = [
        'ID' => "ID",
        'NAME' => "Название",
        'VALUE' => "Значение"
    ];
    public function actions() {
        $this->setPageTitle("Для Инвестора");
        return [
            'index' => "AdminActionUniList",
            'add' => "AdminActionUniListAdd",
            'delete' => "AdminActionUniListDelete",
            'edit' => "AdminActionUniListEdit",
        ];

    }
	/*public function actionIndex()
	{
        //$this->layout = "empty";
		//$this->render('login');
		$this->render('index');
	}*/
}