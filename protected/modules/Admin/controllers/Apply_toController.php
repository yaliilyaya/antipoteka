<?php

class Apply_toController extends AdminController
{
    public $model = "ApplyTo";

    public function actions() {
        $this->setPageTitle("Заявки");
        return [
            'index' => "AdminViewList2Action"
        ];
    }
}