<?php

class PullController extends AdminController
{
    public $model = "Pull";
    public function actions() {
        $this->setPageTitle("Опросы");
        return [
            'index' => "AdminViewListAction",
            'add' => "AdminAddAction",
            'delete' => "AdminDeleteAction",
            'answer_delete' => "AdminDeleteAction",
            'edit' => "AdminEditAction",
            'answer_edit' => "AdminEditAction",
        ];
    }
    public function beforeAction($action) {
        switch($action->getId()) {
            case "answer_delete" :  case "answer_edit" : case "answer" : case "answer_add" :   {
                $this->setPageTitle("Опросы - Ответы");
                $this->model = "PullAnswer";
            }
        }
        return true;
    }
    public function actionIndex() {
        $this->render("index", [
            'list' =>$this->getClass()->findAll()
        ]);
    }
    public function actionAnswer($id) {
        $this->render("answer", [
            'list' =>$this->getClass()->findAll("PULL_ID=:id", ['id' => $id]),
            'pull_id' => $id
        ]);
    }

    public function actionAnswer_add($pull_id) {
        $this->setPageTitle(implode(" - ", [
            $this->getPageTitle(),
            "Добавление"
        ]));
        $model = $this->getClass();
        $attributes = arrVal($_POST, CHtml::modelName($model));
        if ($attributes != null) {
            $model->attributes = $attributes;
            $model->PULL_ID = $pull_id;
            if ($model->save()) {
                $this->redirect($this->url("/admin/{$this->name}/answer/id/{$pull_id}"));
            }
        }
        $this->render("answer_add", [
                'value' =>$model,
                'pull_id' => $pull_id
            ]
        );


    }

}