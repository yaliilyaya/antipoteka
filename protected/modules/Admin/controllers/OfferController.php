<?php

class OfferController extends AdminController
{
    public $model = "Offer";
    public function actions() {
        $this->setPageTitle("Предложения");
        return [
            'index' => "AdminViewListAction",
            'add' => "AdminAddAction",
            'delete' => "AdminDeleteAction",
            'img_delete' => "AdminDeleteAction",
            'edit' => "AdminEditAction",

        ];
    }
    public function beforeAction($action) {
        switch($action->getId()) {
            case "img_delete" :  case "gallery" : case "img_add" :   {
            $this->setPageTitle("Предложения - Галерея");
            $this->model = "OfferImg";
        }
        }
        return true;
    }
    public function actionIndex() {
        $this->render("index", [
            'list' =>$this->getClass()->findAll()
        ]);
    }
    public function actionGallery($id) {
        $this->render("gallery", [
            'list' =>$this->getClass()->findAll("OFFER_ID=:id", ['id' => $id]),
            'offer_id' => $id
        ]);
    }
    public function actionImg_add($offer_id) {
        $this->setPageTitle(implode(" - ", [
            $this->getPageTitle(),
            "Добавление"
        ]));
        $model = $this->getClass();
        $attributes = arrVal($_POST, CHtml::modelName($model));
        if ($attributes != null) {
            $model->attributes = $attributes;
            $model->OFFER_ID = $offer_id;
            if ($model->save()) {
                $this->redirect($this->url("/admin/{$this->name}/gallery/id/{$offer_id}"));
            }
        }
        $this->render("img_add", [
                'value' =>$model,
                'offer_id' => $offer_id
            ]
        );
    }
}