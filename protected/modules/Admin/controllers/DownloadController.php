<?php

class DownloadController extends AdminController
{
    public $model = "Download";
    public function actions() {
        $this->setPageTitle("Загружаемые файлы");
        return [
            'index' => "AdminViewListAction",
            'add' => "AdminAddAction",
            'delete' => "AdminDeleteAction",
            'edit' => "AdminEditAction",
        ];
    }
}