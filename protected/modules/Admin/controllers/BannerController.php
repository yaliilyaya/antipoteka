<?php

class BannerController extends AdminController
{
    public $model = "Banner";

    public function actions() {
        $this->setPageTitle("Баннеры");
        return [
            'index' => "AdminViewListAction",
            'add' => "AdminAddAction",
            'delete' => "AdminDeleteAction",
            'edit' => "AdminEditAction",
        ];
    }


}