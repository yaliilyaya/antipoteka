<?php

class MenuController extends Controller
{
	public function actionIndex()
	{
		$this->renderPartial('index');
	}
	public function actionTop()
	{
		$this->renderPartial('top');
	}
	public function actionLeft()
	{
        $list = [
            $this->url('/admin/banner') => ['name' => "Баннеры", 'icon' => "fa-files-o"],
            $this->url('/admin/director') => ['name' => "Директор", 'icon' => "fa-files-o"],
            $this->url('/admin/advances') => ['name' => "Преемущества", 'icon' => "fa-files-o"],
            $this->url('/admin/calculator') => ['name' => "Калькуляторы", 'icon' => "fa-files-o", ],
            $this->url("/admin/demand") => ['name' => "Требования", 'icon' => "fa-files-o"],
            $this->url("/admin/investor") => ['name' => "Для инвестора", 'icon' => "fa-files-o"],
            $this->url("/admin/contact") => ['name' => "Контакты", 'icon' => "fa-files-o"],

            $this->url("/admin/recall") => ['name' => "Отзывы", 'icon' => "fa-files-o"],
            $this->url("/admin/apply_to") => ['name' => "Подать заявку", 'icon' => "fa-files-o"],
            $this->url("/admin/offer") => ['name' => "Предложения", 'icon' => "fa-files-o"],

            $this->url("/admin/download") => ['name' => "Документы", 'icon' => "fa-files-o"],

            $this->url("/admin/question") => ['name' => "Вопросы", 'icon' => "fa-files-o"],
            $this->url("/admin/pull") => ['name' => "Опросы", 'icon' => "fa-files-o"],
        ];


		$this->renderPartial('left', ['list' => $list]);
	}
}