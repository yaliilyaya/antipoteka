<?php

class DemandController extends Controller
{
    public $table = "unilist";
    public $name_list = [
        'ID' => "ID",
        'NAME' => "Название",
        'VALUE' => "Значение"
    ];
    public function actions() {
        $this->setPageTitle("Требования");
        return [
            'index' => "AdminActionUniList",
            'add' => "AdminActionUniListAdd",
            'delete' => "AdminActionUniListDelete",
            'edit' => "AdminActionUniListEdit",
        ];

    }
	/*public function actionIndex()
	{
        //$this->layout = "empty";
		//$this->render('login');
		$this->render('index');
	}*/
}