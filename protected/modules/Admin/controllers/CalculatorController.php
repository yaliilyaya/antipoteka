<?php

class CalculatorController extends AdminController
{
    public $table = "calculator";
    public $name_list = [
        'ID' => "ID",
        'NAME' => "Название",
        'VALUE' => "Значение"
    ];
    public function actions() {
        $this->setPageTitle("Наши контакты");
        /*return [
            'index' => "AdminActionUniList",
            'add' => "AdminActionUniListAdd",
            'delete' => "AdminActionUniListDelete",
            'edit' => "AdminActionUniListEdit",
        ];*/
    }
    public function actionIndex() {
        $this->render("index");
    }
    public function actionAdd() {

    }
    public function actionEdit($id) {

    }
    public function actionDelete($id) {

    }
}