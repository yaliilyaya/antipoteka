<?php

class QuestionController extends AdminController
{
    public $model = "Question";
    public function actions() {
        $this->setPageTitle("F.A.Q.");
        return [
            'index' => "AdminViewListAction",
            'add' => "AdminAddAction",
            'delete' => "AdminDeleteAction",
            'edit' => "AdminEditAction",
        ];
    }
}