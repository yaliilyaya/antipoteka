<?php

class RecallController extends AdminController
{
    public $model = "Recall";
    public function actions() {
        $this->setPageTitle("Отзывы");
        return [
            'index' => "AdminViewListAction",
            'add' => "AdminAddAction",
            'delete' => "AdminDeleteAction",
            'edit' => "AdminEditAction",
        ];
    }
}