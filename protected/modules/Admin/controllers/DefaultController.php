<?php

class DefaultController extends Controller
{
    public function actions() {
        $this->setPageTitle("Требования");
    }
	public function actionIndex()
	{
        $this->setPageTitle("Админка");
		$this->render('index');
	}
	public function actionLogin()
	{
        $user = arrVal($_POST, "USER");
        if ($user != null ) {
            $identity=new UserIdentity($user["login"], $user["password"]);
            if ($identity->authenticate()) {
                Yii::app()->user->login($identity);
                $this->redirect("/index.php/admin");
            }
        }
        $this->layout = "empty";
        $this->render('login');
	}
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect("/index.php/admin/default/login");
    }

}