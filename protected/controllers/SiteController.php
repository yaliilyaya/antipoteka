<?php
class SiteController extends Controller
{
	public $layout='main';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{

        return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}


	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->setPageTitle("Анти Ипотека");
        $this->render('index',array());
    }

    /**
     *
     */
    public function actionSupply()
    {
        $this->setPageTitle("Наши предложения");
        $this->render('supply', array());
    }

    /**
     *
     */
    public function actionInfo()
    {
        $this->setPageTitle("Информация для инвесторов");
        $this->render('info', array());
    }

    /**
     *
     */
    public function actionContact()
    {
        $this->setPageTitle("Контакты");
        $this->render('contact', array());
    }

    /**
     *
     */
    public function actionFaq()
    {
        $this->setPageTitle("F.A.Q.");
        $this->render('faq', array());
    }

    /**
     * @throws CException
     */
    public function actionRunDownloadFile()
    {
        $this->renderPartial('pages/download_file', [
            'list' => Download::model()->findAll()
        ]);
    }

    /**
     * @throws CException
     */
    public function actionRunInfoMainUser()
    {
        $this->renderPartial('pages/main_user', [
            'value' => Director::model()->findByPk(1)
        ]);
    }

    /**
     * @throws CException
     */
    public function actionRunDemand()
    {
        $this->renderPartial('pages/demand', [
            'list' => Yii::app()->unilist->getDemandValueList()
        ]);
    }

    /**
     * @throws CException
     */
    public function actionRunInvestor()
    {
        $this->renderPartial('pages/investor', [
            'list' => Yii::app()->unilist->getInvestorValueList()
        ]);
    }

    /**
     * @throws CException
     */
    public function actionRunContact()
    {
        $this->renderPartial('pages/contact', [
            'list' => Yii::app()->unilist->getContactValueList()
        ]);
    }

    /**
     * @throws CException
     */
    public function actionRunBanner()
    {

        $this->renderPartial('pages/banner', array(
            "list" => Banner::model()->findAll()
        ));
    }

    /**
     * @throws CException
     */
    public function actionRunVkComment()
    {
        $this->renderPartial('pages/vk_comment', array());
    }

    /**
     * @throws CException
     */
    public function actionRunYaMap()
    {
        $this->renderPartial('pages/yamap', array());
    }
}
