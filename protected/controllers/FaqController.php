<?php

class FaqController extends Controller
{
    /**
     * Lists all models.
     */
    public function actionRunIndex()
    {
        $this->renderPartial('index', [
            'list' => Question::model()->findAll()
        ]);
    }/**
     * Lists all models.
     */
    public function actionRunPoll()
    {
        $run_pull = $this->runPull();
        $pull_list = Pull::model()->findAll();
        foreach ($pull_list as $pull) {
            $pull->ANSWER = PullAnswer::model()->findAll("PULL_ID=:id", [':id' => $pull->ID]);
            foreach ($pull->ANSWER as $answer) {
                $pull->COUNT += $answer->COUNT;
            }
            $pull->RUN = in_array($pull->ID, $run_pull);
        }

        $this->renderPartial('poll', array(
            'pull_list' => $pull_list
        ));
    }
    public function runPull() {
        $run_pull = $this->loadRunPull();
        $post_question = arrVal($_POST, "QUESTION", array());
        if (!count($post_question)) {
            return $run_pull;
        }
        foreach($post_question as $pull_id => $answer_id) {
            if ($this->runAnswer($pull_id, $answer_id)) {
                $this->saveRunPull($pull_id);
                $run_pull[] = $pull_id;
            }
        }

        return $run_pull;
    }
    public function runAnswer($pull_id, $answer_id) {
        $answer = PullAnswer::model()->find(
            "PULL_ID = :pull_id AND ID = :id",
            [':pull_id' => $pull_id, ':id' =>$answer_id]
        );
        if ($answer != null) {
            $answer->COUNT++;
            $answer->save();
            return true;
        }
        return false;
    }
    public function loadRunPull() {
        $run_pull = Yii::app()->session['RUN_PULL'];
        return empty($run_pull) ? array() : $run_pull;
    }
    public function saveRunPull($pull_id) {
        $run_pull = $this->loadRunPull();
        $run_pull[] = $pull_id;
        Yii::app()->session['RUN_PULL'] = $run_pull;

    }
}
