<?php

class AdvancesController extends Controller
{
    /**
     * Lists all models.
     */
    public function actionRunIndex()
    {
        $this->renderPartial('index',[
            'list' => Advances::model()->findAll()
        ]);
    }
}
