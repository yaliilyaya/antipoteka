<?php

class RecallController extends Controller
{
    public function actionRunIndex()
    {
        $this->renderPartial('index', [
            'list' => Recall::model()->findAll()
        ]);
    }

}
