<?php
class ApplyToController extends Controller
{
    /**
     * Lists all models.
     */
    public function actionRunIndex()
    {
        $this->renderPartial('index', array(
            'data' => $this->runApplyTo()
        ));
    }
    /**
     * Lists all models.
     */
    public function actionRunListItem()
    {
        $rs = Offer::model()->findAll();
        $offer_list = [];
        foreach($rs as $offer) {
            $offer_list[$offer->ID] = $offer;
        }
        $offer_img_list = Yii::app()->db->createCommand()
            ->from("offer_img")
            ->where(["IN", "OFFER_ID", array_keys($offer_list)])
            ->group("OFFER_ID")
            ->queryAll();

        foreach ($offer_img_list as $img) {
            $OfferImg = new OfferImg;
            $OfferImg->attributes = $img;
            $offer_list[$img['OFFER_ID']]->IMG = $OfferImg;
        }

        $this->renderPartial('list',array(
            'offer_list' => $offer_list
        ));
    }

    /**
     * index.php/ApplyTo/GetItem/id/6
     * @param $id
     */
    public function actionGetItem($id) {
        $this->layout=false;
        header('Content-type: file/json');
        $offer = Offer::model()->find("ID = :id", [':id' => intval($id)]);
        if ($offer == null) {
            Yii::app()->end();
        }
        $offer->IMG = OfferImg::model()->findAll("OFFER_ID = :offer_id", [':offer_id' => $offer->ID]);

        $offer_data = $offer->getAttributes();
        foreach ($offer->IMG as $img) {
            $offer_data['IMG'][] = $img->getPathFile("IMG");
        }
        echo json_encode($offer_data);
        Yii::app()->end();
    }
    public function actionRunWindowInfo()
    {
        $this->renderPartial('window_info', array());
    }
    public function actionRunFeedback()
    {
        $this->renderPartial('feedback', array(
            'data' => $this->runApplyTo()
        ));
    }
    public function runApplyTo() {
        $apple_to = $this->loadData();
        $apply_to_new = arrVal($_POST, "APPLY_TO", array());
        if (!count($apply_to_new)) {
            return $apple_to;
        }
        Yii::app()->mess->setSuccess("Спасибо, заявка принята.");
        $this->saveData($apply_to_new);
        return $apply_to_new;
    }
    public function loadData() {
        $apple_to = Yii::app()->session['APPLY_TO'];
        return empty($apple_to) ? array() : $apple_to;
    }
    public function saveData($apple_to) {
        Yii::app()->session['APPLY_TO'] = $apple_to;
        $apply_to = new ApplyTo;
        $apply_to->attributes = $apple_to;
        $apply_to->save();
    }

}
