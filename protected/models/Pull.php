<?php

/**
 * This is the model class for table "pull".
 *
 * The followings are the available columns in table 'pull':
 * @property integer $ID
 * @property string $TEXT
 * @property integer $COUNT
 *
 * The followings are the available model relations:
 * @property PullAnswer[] $pullAnswers
 */
class Pull extends AdminActiveRecord
{
    public $ANSWER = null;
    public $RUN = false;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pull';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('COUNT', 'numerical', 'integerOnly'=>true),
			array('TEXT', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, TEXT, COUNT', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pullAnswers' => array(self::HAS_MANY, 'PullAnswer', 'PULL_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'ID' => 'ID',
			'TEXT' => 'Вопрос',
			//'COUNT' => 'Count',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('TEXT',$this->TEXT,true);
		$criteria->compare('COUNT',$this->COUNT);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pull the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
