<?php

/**
 * This is the model class for table "director".
 *
 * The followings are the available columns in table 'director':
 * @property integer $ID
 * @property string $NAME
 * @property string $TYPE
 * @property string $TEXT
 * @property integer $IMG
 */
class Director extends AdminActiveRecord
{
    protected $type = [
        'TEXT' => 'text',
        'IMG' => "file"
    ];
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'director';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('IMG, NAME, TYPE', 'length', 'max'=>255),
			array('TEXT', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, NAME, TYPE, TEXT, IMG', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'ID' => 'ID',
			'NAME' => 'ФИО',
			'TYPE' => 'Должность',
            'IMG' => 'Изображение',
			'TEXT' => 'Текст',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('TYPE',$this->TYPE,true);
		$criteria->compare('TEXT',$this->TEXT,true);
		$criteria->compare('IMG',$this->IMG);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function primaryKey()
    {
        return 'ID';
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Director the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
