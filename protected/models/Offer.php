<?php

/**
 * This is the model class for table "offer".
 *
 * The followings are the available columns in table 'offer':
 * @property integer $ID
 * @property integer $AREA
 * @property integer $PRICE
 * @property integer $PRICE_M
 * @property string $DATA
 * @property string $ADDRESS
 */
class Offer extends AdminActiveRecord
{
    public $IMG;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('AREA, PRICE, PRICE_M', 'numerical', 'integerOnly'=>true),
			array('ADDRESS', 'length', 'max'=>225),
			array('DATA', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, AREA, PRICE, PRICE_M, DATA, ADDRESS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'ID' => 'ID',
            'ADDRESS' => 'Адресс',
			'AREA' => 'Площадь',
			'PRICE' => 'Цена',
			'PRICE_M' => 'Цена за метр',
			//'DATA' => 'Data',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('AREA',$this->AREA);
		$criteria->compare('PRICE',$this->PRICE);
		$criteria->compare('PRICE_M',$this->PRICE_M);
		$criteria->compare('DATA',$this->DATA,true);
		$criteria->compare('ADDRESS',$this->ADDRESS,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Offer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
