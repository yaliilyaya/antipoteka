<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 29.06.15
 * Time: 1:31
 */

/**
 * Class Action
 * @property Controller $controller
 */
class Action extends CAction {

    public function __get($name) {
        if ($name == "controller") {
            return $this->getController();
        }
        return;
    }
    public function url($route,$params=array(),$ampersand='&') {
        return $this->controller->createUrl($route, $params, $ampersand);
    }
} 