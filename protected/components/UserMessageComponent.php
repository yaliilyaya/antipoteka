<?php
class UserMessageComponent extends CComponent {
    const TYPE_DEF = "def",
          TYPE_ERROR = "error",
          TYPE_SUCCESS = "success";

    private $mess = array();
    public function init() {

    }
    public function setMessage($mess, $type = self::TYPE_DEF) {
        $this->mess[$type][] = $mess;
    }
    public function setSuccess($mess) {
        $this->setMessage($mess, self::TYPE_SUCCESS);
    }
    public function setError($mess) {
        $this->setMessage($mess, self::TYPE_ERROR);
    }
    public function getMessage($type = self::TYPE_DEF) {
        return arrVal($this->mess, $type, array());
    }
    public function getError() {
        return $this->getMessage(self::TYPE_ERROR);
    }
    public function getSuccess() {
        return $this->getMessage(self::TYPE_SUCCESS);
    }
    public function isMessage($type = self::TYPE_DEF) {
        return (isset($this->mess[$type]));
    }
    public function isError() {
        return $this->isMessage(self::TYPE_ERROR);
    }
    public function isSuccess() {
        return $this->isMessage(self::TYPE_SUCCESS);
    }
    public function printMessage($mess, $type) {
        ?>
        <script>
            $(function() {
                WindowMessage.<?=$type?>("<?=implode("<br>", $mess)?>").open();
            })
        </script>
        <?
    }
    public function printSuccess($mess) {
        $this->printMessage($mess, self::TYPE_SUCCESS);
    }
    public function printError($mess) {
        $this->printMessage($mess, self::TYPE_ERROR);
    }


} 