<?class UniListComponent extends CComponent {
    private $table = "unilist";
    public function init() {
    }
    public function getValueList($type) {
        $query = Yii::app()->db->createCommand()
            ->select(["ID", "NAME", "VALUE"])
            ->from($this->table)
            ->where(["AND", "TYPE = :type"], [':type'=> $type]);
        return $query->queryAll();
    }
    public function getDemandValueList() {
        return $this->getValueList("demand");
    }
    public function getInvestorValueList() {
        return $this->getValueList("investor");
    }
    public function getContactValueList() {
        return $this->getValueList("contact");
    }
}