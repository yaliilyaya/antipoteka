<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 * @property string $name
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public function __construct($id,$module) {
        parent::__construct($id, $module);

        $js = '/js/'.$this->name.'.js';
        if (file_exists(DIR_ROOT.$js)) {
            $this->registerJs($js);
        }
        $css = '/css/'.$this->name.'.css';
        if (file_exists(DIR_ROOT.$css)) {
            $this->registerCss($css);
        }
    }
    public function registerJs($path, $inner = true) {
        $cs = Yii::app()->getClientScript();

        $baseUrl = ($inner? Yii::app()->baseUrl : "");
        $cs->registerScriptFile($baseUrl.$path);
    }
    public function registerCss($path, $inner = true) {
        $cs = Yii::app()->getClientScript();
        $baseUrl = ($inner? Yii::app()->baseUrl : "");
        $cs->registerCssFile($baseUrl.$path);
    }
    public function getName() {
        return strtolower($this->getId());
    }
    public function __get($name) {
        if ($name == 'name') {
            return $this->getName();
        }
        return null;
    }
    public function url($route,$params=array(),$ampersand='&') {
        return $this->createUrl($route, $params, $ampersand);
    }
}
function arrVal(&$arr, $key, $default = null) {
    return (isset($arr[$key]) ? $arr[$key] : $default);
}