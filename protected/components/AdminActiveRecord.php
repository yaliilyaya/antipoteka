<?php
/**
 * Created by PhpStorm.
 * User: yali
 * Date: 01.07.15
 * Time: 0:08
 */

class AdminActiveRecord extends CActiveRecord {
    protected $type = [];
    public function getType($attribute) {
        return arrVal($this->type, $attribute);
    }
    public function getAttributeByType($type) {
        $result = [];
        foreach ($this->type as $field => $type_field) {
            if ($type_field === $type) {
                $result[] = $field;
            }
        }
        return $result;
    }
    public function beforeSave() {
        foreach ($this->getAttributeByType("file") as $attribute) {
            if (!$this->hasAttribute($attribute)) {
                continue;
            }
            $temp = $this->$attribute;
            $this->$attribute=CUploadedFile::getInstance($this, $attribute);
            if (empty($this->$attribute)) {
                $this->$attribute = $temp;
                continue;
            }
            if ($this->validate($attribute)) {
                $file_name = $this->getFileName($this->$attribute);
                $this->$attribute->saveAs(Yii::getPathOfAlias('webroot').'/upload/'.$file_name);
                $this->$attribute = $file_name;
            }
            else {
                return false;
            }
        }
        return true;
    }
    protected function getFileName(CUploadedFile $file) {
        return md5(date("d.m.Y_H:i:s")).".".$this->getTypeFile($file);
    }
    protected function getTypeFile(CUploadedFile $file) {
        $type = explode("/", $file->getType());
        return $type[count($type) - 1];
    }
    public function getPathFile($attribute) {
        return '/upload/'.$this->$attribute;
    }
} 